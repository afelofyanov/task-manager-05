package ru.tsc.felofyanov.tm;

import ru.tsc.felofyanov.tm.constant.TerminalConst;

public final class Application {

    public static void main(String[] args) {
        process(args);
    }

    public static void process(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            default:
                showError(arg);
                break;
        }
    }

    public static void showError(String arg) {
        System.err.printf("Error! This argument '%s' not supported...\n", arg);
    }

    public static void showAbout() {
        System.out.println("[About]");
        System.out.println("Name: Alexander Felofyanov");
        System.out.println("E-mail: afelofyanov@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("1.5.0");
    }

    public static void showHelp() {
        System.out.println("[Help]");
        System.out.format("%s - Show developer info. \n", TerminalConst.ABOUT);
        System.out.format("%s - Show applicant version. \n", TerminalConst.VERSION);
        System.out.format("%s - Show terminal commands info. \n", TerminalConst.HELP);
    }
}
